package com.example.recuperacionc1;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageView;

public class CuentaBancoActivity extends AppCompatActivity {

    private EditText etCantidad, etNumeroCuenta, etNombreCliente, etBanco, etSaldo;
    private TextView tvNuevoSaldo, tvUsuario;
    private Button btnAplicarMovimiento, btnRegistrar, btnSalir;
    private RadioGroup rgMovimientos;
    private RadioButton rbDeposito, rbRetiro, rbConsultarSaldo;
    private CuentaBanco cuentaBanco;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuenta_banco);

        etCantidad = findViewById(R.id.etCantidad);
        etNumeroCuenta = findViewById(R.id.etNumeroCuenta);
        etNombreCliente = findViewById(R.id.etNombreCliente);
        etBanco = findViewById(R.id.etBanco);
        etSaldo = findViewById(R.id.etSaldo);
        tvNuevoSaldo = findViewById(R.id.tvNuevoSaldo);
        tvUsuario = findViewById(R.id.tvUsuario);
        btnAplicarMovimiento = findViewById(R.id.btnAplicarMovimiento);
        btnRegistrar = findViewById(R.id.btnRegistrar);
        btnSalir = findViewById(R.id.btnSalir);
        rgMovimientos = findViewById(R.id.rgMovimientos);
        rbDeposito = findViewById(R.id.rbDeposito);
        rbRetiro = findViewById(R.id.rbRetiro);
        rbConsultarSaldo = findViewById(R.id.rbConsultarSaldo);

        ImageView logoImageView = findViewById(R.id.logoImageView);
        logoImageView.setImageResource(R.drawable.img);

        // Obtener el usuario de la actividad anterior
        String usuario = getIntent().getStringExtra("usuario");
        tvUsuario.setText("Usuario: " + usuario);

        // Inicialmente la cuenta está vacía
        cuentaBanco = null;

        btnAplicarMovimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aplicarMovimiento();
            }
        });

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrarCuenta();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salir();
            }
        });
    }

    private void aplicarMovimiento() {
        if (cuentaBanco == null) {
            Toast.makeText(this, "Debe registrar una cuenta primero", Toast.LENGTH_SHORT).show();
            return;
        }

        int selectedId = rgMovimientos.getCheckedRadioButtonId();

        if (selectedId == -1) {
            Toast.makeText(this, "Debe seleccionar un tipo de movimiento", Toast.LENGTH_SHORT).show();
            return;
        }

        if (selectedId == R.id.rbConsultarSaldo) {
            tvNuevoSaldo.setText("Nuevo saldo: " + cuentaBanco.consultarSaldo());
            etCantidad.setText("");  // Clear the input field
            return;
        }

        String cantidadStr = etCantidad.getText().toString();
        if (cantidadStr.isEmpty()) {
            Toast.makeText(this, "Debe ingresar una cantidad", Toast.LENGTH_SHORT).show();
            return;
        }

        double cantidad = Double.parseDouble(cantidadStr);

        if (selectedId == R.id.rbDeposito) {
            cuentaBanco.depositar(cantidad);
            tvNuevoSaldo.setText("Nuevo saldo: " + cuentaBanco.consultarSaldo());
        } else if (selectedId == R.id.rbRetiro) {
            if (cuentaBanco.retirar(cantidad)) {
                tvNuevoSaldo.setText("Nuevo saldo: " + cuentaBanco.consultarSaldo());
            } else {
                Toast.makeText(this, "Saldo insuficiente", Toast.LENGTH_SHORT).show();
            }
        }

        etCantidad.setText("");  // Clear the input field after deposit/withdraw
    }

    private void registrarCuenta() {
        String numeroCuenta = etNumeroCuenta.getText().toString();
        String nombreCliente = etNombreCliente.getText().toString();
        String banco = getString(R.string.banco);
        String saldoStr = etSaldo.getText().toString();

        if (numeroCuenta.isEmpty() || nombreCliente.isEmpty() || saldoStr.isEmpty()) {
            Toast.makeText(this, "Todos los campos son requeridos", Toast.LENGTH_SHORT).show();
            return;
        }

        double saldo = Double.parseDouble(saldoStr);

        // Crear una nueva cuenta bancaria
        cuentaBanco = new CuentaBanco(numeroCuenta, nombreCliente, banco, saldo);

        Toast.makeText(this, "Cuenta registrada con éxito", Toast.LENGTH_SHORT).show();
    }

    private void salir() {
        finish(); // Cierra la actividad actual y vuelve a la actividad anterior
    }
}


