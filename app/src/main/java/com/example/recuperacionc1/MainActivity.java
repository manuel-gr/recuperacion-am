package com.example.recuperacionc1;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText etUsuario, etContrasena;
    private Button btnIngresar, btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etUsuario = findViewById(R.id.etUsuario);
        etContrasena = findViewById(R.id.etContrasena);
        btnIngresar = findViewById(R.id.btnIngresar);
        btnSalir = findViewById(R.id.btnSalir);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingresar();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salir();
            }
        });
    }

    private void ingresar() {
        String usuario = etUsuario.getText().toString();
        String contrasena = etContrasena.getText().toString();

        if (usuario.isEmpty() || contrasena.isEmpty()) {
            Toast.makeText(this, "Usuario y contraseña son requeridos", Toast.LENGTH_SHORT).show();
            return;
        }

        // Comprobar usuario y contraseña con los recursos String
        String usuarioResource = getString(R.string.user);
        String contrasenaResource = getString(R.string.pass);

        if (usuario.equals(usuarioResource) && contrasena.equals(contrasenaResource)) {
            Intent intent = new Intent(this, CuentaBancoActivity.class);
            intent.putExtra("usuario", usuario);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Usuario o contraseña incorrectos", Toast.LENGTH_SHORT).show();
        }
    }

    private void salir() {
        finishAffinity(); // Cierra la aplicación
    }
}

